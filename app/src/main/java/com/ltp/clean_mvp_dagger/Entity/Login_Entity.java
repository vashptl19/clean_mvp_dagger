package com.ltp.clean_mvp_dagger.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ltp.clean_mvp_dagger.Model.LoginModel;

import java.util.List;

/**
 * Created by vcollabera-vp on 27/02/18.
 */

public class Login_Entity {

    @SerializedName("Login")
    @Expose
    private List<LoginModel> login = null;


    public List<LoginModel> getLogin() {
        return login;
    }

    public void setLogin(List<LoginModel> login) {
        this.login = login;
    }
}
