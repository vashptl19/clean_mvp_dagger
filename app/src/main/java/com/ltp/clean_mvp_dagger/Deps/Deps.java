package com.ltp.clean_mvp_dagger.Deps;

import com.ltp.clean_mvp_dagger.BaseApp;
import com.ltp.clean_mvp_dagger.Home.HomeActivity;
import com.ltp.clean_mvp_dagger.Networking.AppModule;
import com.ltp.clean_mvp_dagger.Networking.NetModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {NetModule.class, AppModule.class})
public interface Deps {
    Retrofit RETROFIT();

    void inject(BaseApp app);

    void inject(HomeActivity homeActivity);
}
