package com.ltp.clean_mvp_dagger.Utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.ltp.clean_mvp_dagger.R;

import java.io.ByteArrayOutputStream;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vash on 12/10/17.
 */

public class Constants {
    //    public static String mBaseurl = "http://10.11.3.170:8072/LoginWebService/LoginWebService.asmx/";
    public static String mBaseurl = "http://adminpanel.shukaninsurance.com/webmethods/WebService.asmx/";
    public static AlertDialog alertDialog;
    public static ProgressDialog dialog;
    //Intent//
    public static String isFrom = "isFrom";

    // SharedPrefs //
    public static String psnNO = "psnNo";
    public static String pass = "pass";

    // SharedPrefs //

    //Bundle//
    //Bundle//
    //Intent//
    public static Retrofit retrofit;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            /*for (int i = 0; i < okHttpClient.protocols().size(); i++) {
                okHttpClient.protocols().set(i, Protocol.HTTP_1_1);
            }*/

            retrofit = new Retrofit.Builder()
                    .baseUrl(mBaseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            Log.e("> PROTOCOLS ", "> " + new Gson().toJson(new OkHttpClient().protocols()));
        }
        return retrofit;
    }

    public static String bmpTObase64(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    public static void Loading(Context context) {

        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading!!");
        dialog.setTitle("Please Wait");
        dialog.setCancelable(false);
        dialog.show();

//        dialog = new Dialog(context);
//        dialog.setContentView(R.layout.loading_layout);
//        ImageView iv_LoadingLogo = (ImageView) dialog.findViewById(R.id.loading_icon);
//        iv_LoadingLogo.setImageResource(R.drawable.ic_logo_colored);
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(null);
//        dialog.show();
//        dialog.findViewById(R.id.loading_icon).startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_360));
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            bitmap.setWidth(100);
            bitmap.setHeight(100);
        }*/

        return bitmap;

    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
            // connected to the internet
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {

                return true;

            } else if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        } else {
            // not connected to the internet
            return false;
        }

        return false;

    }

    public static void SingleActionDialog(final Context context, String title, String message, String btnTxt, DialogInterface.OnClickListener positiveListener) {

        SpannableString webStr = new SpannableString(message);
        Linkify.addLinks(webStr, Linkify.EMAIL_ADDRESSES);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(webStr)
                .setCancelable(false)
                .setPositiveButton(btnTxt, positiveListener);

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        });

        // show it
        alertDialog.show();

    }

    // Single Action Alert Dialog //

    // Double Action Alert Dialog //

    public static void DoubleActionDialog(final Context context, String title, String message, String btnPosTxt, String btnNegTxt, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btnPosTxt, positiveListener)
                .setNegativeButton(btnNegTxt, negativeListener);

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        });

        // show it
        alertDialog.show();
    }

    // Double Action Alert Dialog //
    // Triple Action Alert Dialog //

    public static void TripleActionDialog(final Context context, String title, String message, String btnPosTxt, String btnPosTxt1, String btnNegTxt, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener, DialogInterface.OnCancelListener onDismissListener) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btnPosTxt, positiveListener)
                .setNegativeButton(btnNegTxt, negativeListener)
                .setOnCancelListener(onDismissListener);

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        });

        // show it
        alertDialog.show();
    }

    // Triple Action Alert Dialog //


    public static void NetworkAlertDialog(final Context context, DialogInterface.OnClickListener trybtn, DialogInterface.OnClickListener settingbtn, DialogInterface.OnClickListener cancelBtn) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle("Network Problem");
        alertDialogBuilder.setMessage("Please check your Internet connection. Make sure you are connected to a WiFi network or have Mobile Data switched on.")
                .setCancelable(false)
                .setPositiveButton("Try Again", trybtn)
                .setPositiveButton("Settings", settingbtn)
                .setNegativeButton("Cancel", cancelBtn);

        // create alert dialog
        alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        });

        // show it
        alertDialog.show();
    }

    // Preference Manager Methods //

}
