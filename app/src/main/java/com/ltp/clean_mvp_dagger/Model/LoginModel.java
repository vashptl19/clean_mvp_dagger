package com.ltp.clean_mvp_dagger.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("OrganisationID")
    @Expose
    private Integer organisationID;
    @SerializedName("BranchId")
    @Expose
    private Object branchId;
    @SerializedName("AgentId")
    @Expose
    private Object agentId;
    @SerializedName("RoleID")
    @Expose
    private Integer roleID;
    @SerializedName("Code")
    @Expose
    private Object code;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private Object middleName;
    @SerializedName("LastName")
    @Expose
    private Object lastName;
    @SerializedName("Photo")
    @Expose
    private Object photo;
    @SerializedName("Address")
    @Expose
    private Object address;
    @SerializedName("City")
    @Expose
    private Object city;
    @SerializedName("State")
    @Expose
    private Object state;
    @SerializedName("PostalCode")
    @Expose
    private Object postalCode;
    @SerializedName("DateOfBirth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("DateOfJoin")
    @Expose
    private Object dateOfJoin;
    @SerializedName("EmailId")
    @Expose
    private Object emailId;
    @SerializedName("ContactNo")
    @Expose
    private Object contactNo;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("IsResigned")
    @Expose
    private Object isResigned;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("CreatedBy")
    @Expose
    private Object createdBy;
    @SerializedName("CreatedDate")
    @Expose
    private Object createdDate;
    @SerializedName("LastModifiedBy")
    @Expose
    private Object lastModifiedBy;
    @SerializedName("LastModifiedDate")
    @Expose
    private Object lastModifiedDate;
    @SerializedName("OrganisationName")
    @Expose
    private String organisationName;
    @SerializedName("AccountName")
    @Expose
    private Object accountName;
    @SerializedName("AccountNo")
    @Expose
    private Object accountNo;
    @SerializedName("BankName")
    @Expose
    private Object bankName;
    @SerializedName("CompanyRegNo")
    @Expose
    private String companyRegNo;
    @SerializedName("Mobile")
    @Expose
    private Object mobile;
    @SerializedName("Mobile2")
    @Expose
    private String mobile2;
    @SerializedName("SortCode")
    @Expose
    private Object sortCode;
    @SerializedName("Telephone")
    @Expose
    private Object telephone;
    @SerializedName("VATNo")
    @Expose
    private Object vATNo;

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public Integer getOrganisationID() {
        return organisationID;
    }

    public void setOrganisationID(Integer organisationID) {
        this.organisationID = organisationID;
    }

    public Object getBranchId() {
        return branchId;
    }

    public void setBranchId(Object branchId) {
        this.branchId = branchId;
    }

    public Object getAgentId() {
        return agentId;
    }

    public void setAgentId(Object agentId) {
        this.agentId = agentId;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getMiddleName() {
        return middleName;
    }

    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Object postalCode) {
        this.postalCode = postalCode;
    }

    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Object getDateOfJoin() {
        return dateOfJoin;
    }

    public void setDateOfJoin(Object dateOfJoin) {
        this.dateOfJoin = dateOfJoin;
    }

    public Object getEmailId() {
        return emailId;
    }

    public void setEmailId(Object emailId) {
        this.emailId = emailId;
    }

    public Object getContactNo() {
        return contactNo;
    }

    public void setContactNo(Object contactNo) {
        this.contactNo = contactNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getIsResigned() {
        return isResigned;
    }

    public void setIsResigned(Object isResigned) {
        this.isResigned = isResigned;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Object lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Object getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Object lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public Object getAccountName() {
        return accountName;
    }

    public void setAccountName(Object accountName) {
        this.accountName = accountName;
    }

    public Object getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Object accountNo) {
        this.accountNo = accountNo;
    }

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public String getCompanyRegNo() {
        return companyRegNo;
    }

    public void setCompanyRegNo(String companyRegNo) {
        this.companyRegNo = companyRegNo;
    }

    public Object getMobile() {
        return mobile;
    }

    public void setMobile(Object mobile) {
        this.mobile = mobile;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public Object getSortCode() {
        return sortCode;
    }

    public void setSortCode(Object sortCode) {
        this.sortCode = sortCode;
    }

    public Object getTelephone() {
        return telephone;
    }

    public void setTelephone(Object telephone) {
        this.telephone = telephone;
    }

    public Object getvATNo() {
        return vATNo;
    }

    public void setvATNo(Object vATNo) {
        this.vATNo = vATNo;
    }
}
