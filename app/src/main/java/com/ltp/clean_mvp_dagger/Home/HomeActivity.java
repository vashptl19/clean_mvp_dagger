package com.ltp.clean_mvp_dagger.Home;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ltp.clean_mvp_dagger.BaseApp;
import com.ltp.clean_mvp_dagger.Commons.CommonActivity;
import com.ltp.clean_mvp_dagger.Entity.Login_Entity;
import com.ltp.clean_mvp_dagger.Networking.Service;
import com.ltp.clean_mvp_dagger.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;

public class HomeActivity extends CommonActivity implements HomeView {
    @Inject
    Retrofit retrofit;
    @Inject
    SharedPreferences preferences;
    @Inject
    Service service;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.progress)
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        ((BaseApp) getApplicationContext()).getDeps().inject(this);

        HomePresenter homePresenter = new HomePresenter(service, this, "80000134450", "Welcome@123");
        homePresenter.getLogin();
    }

    @Override
    public void init() {
        super.init();
        list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void showWait() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getLoginSuccess(Login_Entity login_entity) {
        Toast.makeText(this, ". S> " + login_entity.getLogin().get(0).getFirstName(), Toast.LENGTH_SHORT).show();
    /*    HomeAdapter adapter = new HomeAdapter(getApplicationContext(), login_entity.getLogin(),
                new HomeAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(LoginModel Item) {
                        Toast.makeText(getApplicationContext(), Item.getFirstName(), Toast.LENGTH_LONG).show();
                    }
                });
        list.setAdapter(adapter);*/
    }
}
