package com.ltp.clean_mvp_dagger.Home;

import com.ltp.clean_mvp_dagger.Entity.Login_Entity;

public interface HomeView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getLoginSuccess(Login_Entity login_entity);

}