package com.ltp.clean_mvp_dagger.Home;


import com.ltp.clean_mvp_dagger.Entity.Login_Entity;
import com.ltp.clean_mvp_dagger.Networking.NetworkError;
import com.ltp.clean_mvp_dagger.Networking.Service;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ennur on 6/25/16.
 */
public class HomePresenter {
    private final Service service;
    private final HomeView view;
    private CompositeSubscription subscriptions;
    private String uname;
    private String pass;

    public HomePresenter(Service service, HomeView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
    }

    public HomePresenter(Service service, HomeView view, String name, String pass) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.uname = name;
        this.pass = pass;
    }

    public void getLogin() {
        view.showWait();

        Subscription subscription = service.getLogin(new Service.ServiceCallback() {
            @Override
            public void onSuccess(Login_Entity login_entity) {
                view.removeWait();
                view.getLoginSuccess(login_entity);
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
                view.onFailure(networkError.getAppErrorMessage());
            }
        }, uname, pass);

        subscriptions.add(subscription);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }
}
