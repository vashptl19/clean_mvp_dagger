package com.ltp.clean_mvp_dagger.Commons;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.InflateException;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.ltp.clean_mvp_dagger.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;


public abstract class CommonActivity extends AppCompatActivity implements Validator.ValidationListener {
    protected Validator validator;

    protected Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
    }

    @Override
    public void onValidationFailed(@NonNull List<ValidationError> errors) {
        for (ValidationError error : errors) {
            final View view = error.getView();
            //String message = error.getCollatedErrorMessage(this);
            try {
                if (view != null) {
                    Log.e("> TIL ", ">til " + (view instanceof TextInputLayout) + "\n tet " + (view instanceof TextInputEditText) + "\n>ET " + (view instanceof EditText));

                    if (view instanceof EditText) {
                        ((EditText) view).setError("This field is required.");

                    } else if (view instanceof TextInputLayout) {
//                        ((TextInputLayout) view).setEnabled(true);
//                        ((TextInputLayout) view).setError("This field is required.");
//                        ((TextInputEditText) view).setError("This field is required.");
                    } else {
                        //Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (InflateException e) {
                Log.e("log_tag", "Inflate Exception");
                //Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            } catch (Exception e) {

            }

            // Display error messages ;)

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void goTo(final Class aClass) {
        startActivity(new Intent(this, aClass));
    }

    @Override
    public void onValidationSucceeded() {

    }

    public void init() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    public void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }


}
