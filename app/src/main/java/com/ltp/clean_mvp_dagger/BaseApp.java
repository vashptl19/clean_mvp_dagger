package com.ltp.clean_mvp_dagger;

import android.app.Application;

import com.ltp.clean_mvp_dagger.Deps.DaggerDeps;
import com.ltp.clean_mvp_dagger.Deps.Deps;
import com.ltp.clean_mvp_dagger.Networking.AppModule;
import com.ltp.clean_mvp_dagger.Networking.NetModule;

/**
 * Created by vcollabera-vp on 01/03/18.
 */

public class BaseApp extends Application {

    private Deps deps;

    @Override
    public void onCreate() {
        super.onCreate();
        deps = DaggerDeps.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .build();
        deps.inject(this);
    }

    public Deps getDeps() {
        return deps;
    }
}