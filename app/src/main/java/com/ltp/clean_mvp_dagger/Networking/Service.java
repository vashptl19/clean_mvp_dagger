package com.ltp.clean_mvp_dagger.Networking;

import com.ltp.clean_mvp_dagger.Entity.Login_Entity;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getLogin(final ServiceCallback callback, String uName, String pass) {

        return networkService.getLogin(uName, pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends Login_Entity>>() {
                    @Override
                    public Observable<? extends Login_Entity> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<Login_Entity>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(Login_Entity login_entity) {
                        callback.onSuccess(login_entity);

                    }

                });
    }


    public interface ServiceCallback {
        void onSuccess(Login_Entity login_entity);

        void onError(NetworkError networkError);
    }
}