package com.ltp.clean_mvp_dagger.Networking;


import com.ltp.clean_mvp_dagger.BaseApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vash on 12/10/17.
 */
@Module
public class AppModule {
    BaseApp app;

    public AppModule(BaseApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    BaseApp provideApp() {
        return app;
    }

}
