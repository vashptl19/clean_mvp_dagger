package com.ltp.clean_mvp_dagger.Networking;


import com.ltp.clean_mvp_dagger.Entity.Login_Entity;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by ennur on 6/25/16.
 */
public interface NetworkService {

    @GET("UserLogin")
    Observable<Login_Entity> getLogin(@Query("UserName") String userName, @Query("Password") String pass);

}
